#ifndef __ElossAnalysis_h
#define __ElossAnalysis_h

#include <iostream>

#include "Analysis.h"
#include "Eloss.h"

class ElossAnalysis : public Analysis { 
 public: 
  ElossAnalysis(Eloss *eIn);
  ~ElossAnalysis();

  void Init();
  void Process(); 
  void Term();
 protected: 
  Eloss *e;
};

#endif
