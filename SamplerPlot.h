#ifndef __SamplerPlot_h
#define __SamplerPlot_h

#include "TH1.h"
#include "TH2.h"

#include "Sampler.h"

class SamplerPlot { 
 public :
  std::string histName;
  std::string var1Name;
  std::string var2Name;

  int         partID;

  int         nXBins;
  double      dXLow;
  double      dXHigh;

  int         nYBins;
  double      dYLow;
  double      dYHigh;

  std::string options;

  TH1D*       h1;
  TH2D*       h2;
  
  Sampler     *s;
  float       *var1;
  float       *var2;

  SamplerPlot(std::string histNameIn, std::string var1NameIn, std::string var2NameIn,
	      int partIDIn, 
	      int nXBinsIn, double dXLowIn, double dXHighIn, 
	      int nYBinsIn, double dYLowIn, double dYHighIn, 
	      std::string optionsIn,Sampler *sIn);
  void Fill();
  TH1* GetHisto();
};


#endif
