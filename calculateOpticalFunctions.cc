#include "OutputOptics.h"
#include "RobdsimAnalysis.h"
#include "RobdsimOutput.h"
#include "Sampler.h"
#include "SamplerAnalysis.h"

#include "TFile.h"
#include "TList.h"
#include "TTree.h"
#include "TFile.h"

#include <iostream>
#include <string>
#include <vector>

void usage();

int main(int argc, char* argv[])
{
  if(argc < 3)
    {
      std::cout << "Insufficent number of arguments!" << std::endl;
      usage();
      exit(1);
    }

  // parse arguments to program
  std::string fileName       = std::string(argv[1]);
  std::string outputFileName = std::string(argv[2]);
  bool debug = false;
  if (argc > 3)
    {debug = true;}

  // open file and get a list of all things inside it
  TFile* file = new TFile(fileName.c_str());

  if (file->IsZombie()) {
    file->Close();
    exit(1);
  }

  // create output
  std::string rootOutputFileName  = outputFileName + ".root";
  std::string asciiOutputFileName = outputFileName + ".dat";
  TFile* frout = new TFile(rootOutputFileName.c_str(),"RECREATE"); 
  OutputOptics* outputOptics = new OutputOptics(asciiOutputFileName, debug);
  //frout->AddDirectory(outputOptics->GetTTree()); 

  TList* keys = file->GetListOfKeys();
  int nSamplers = keys->GetEntries();

  // make a vector of only the sampler trees in the file
  std::vector<std::string> samplerNames;
  for(int i = 0; i < nSamplers; ++i)
    {samplerNames.push_back(keys->At(i)->GetName());}

  // for each sampler, calculate the optical functions and write them to file
  int i = 0;
  for (auto key : samplerNames)
    {
      //some nice feedback
      std::cout << "\r Sampler> " << std::setprecision(1) << std::fixed << i+1 << " of " << nSamplers;
      std::cout.flush();
      i++;
      
      TObject* object        = file->Get(key.c_str());
      std::string className  = std::string(object->ClassName());
      bool isSampler         = true;
      bool isEloss           = key.find("ElossTr") != std::string::npos;
      bool isPloss           = key.find("PlossTr") != std::string::npos;
      bool isPhits           = key.find("PhitsTr") != std::string::npos;
      bool isTunnel          = key.find("TunnelHitsTr") != std::string::npos;
      bool isPrimary         = key.find("rimar")   != std::string::npos; // could have small or capital p or could be plural
      bool isTree            = className == "TTree";

      if (isEloss || isPloss || isPhits || isTunnel) //Everything else is a sampler.
        {isSampler = false;}

      if (debug)
	{
	  std::cout << "object: " << key << " of class: " << className << std::endl;
	  std::cout << "Tree? " << isTree << " Sampler? " << isSampler << " Primary? " << isPrimary << std::endl;
	}
      
      if (isTree && (isPrimary || isSampler) )
	{
	  if (debug)
	    {std::cout << "Analysing" << std::endl;}

	  TTree* tree = dynamic_cast<TTree*>(object);
	  if (tree)
	    {
	      Sampler* s         = new Sampler(tree, false);
	      SamplerAnalysis* a = new SamplerAnalysis(s,debug);
	      a->CalculateOpticalFunctions();
	      outputOptics->AppendOpticsEntry(key,a->GetOpticalFunctions());
	      delete a;
	      delete s;
	      delete tree;
	    }
	  else
	    {std::cerr << std::endl << "Class name tree but doesn't successfully cast - skipping" << std::endl;}
	}
      //outputOptics->GetTTree()->Write();
    }
  std::cout << std::endl;

  outputOptics->CloseFile();
  file->Close();
  frout->Write();
  frout->Close();

  delete frout;

  delete file;
  delete outputOptics;

  return 0;
}

void usage()
{ 
  std::cout << "usage: calculateOpticalFunctions <dataFile> <outputfile>" << std::endl;
  std::cout << " <datafile>   - root file to operate on ie run1.root"     << std::endl;
  std::cout << " <outputfile> - name of output file ie optics.dat"        << std::endl;
}
