#ifndef __OutputOptics_h
#define __OutputOptics_h

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>

#include "TTree.h"

class OutputOptics
{
 public:
  OutputOptics(std::string outputFilename, bool debugIn = false);
  ~OutputOptics();

  void   MakeAscii();
  void   MakeTTree();

  void   AppendOpticsEntry(std::string nameIn, std::vector<double> opticsIn);

  TTree* GetTTree(); 

  void   CloseFile();


 private:
  // variable sfor the ascii output
  std::ofstream of;
  std::string filename;
  bool        debug;


  TTree *tree;
  // variables for the tree
  double S = 0;
  double Beta_x;
  double Beta_y;
  double Alph_x;
  double Alph_y;
  double Disp_x;
  double Disp_y;
  double Disp_xp;
  double Disp_yp;
  double Emitt_x;
  double Emitt_y;
  double Sigma_x;
  double Sigma_y;
  double Sigma_xp;
  double Sigma_yp;
  double Sigma_x_xp;
  double Sigma_y_yp;
  double Mean_x;
  double Mean_y;
  double Mean_xp;
  double Mean_yp;
  double Sigma_mean_x;
  double Sigma_mean_y;
  double Sigma_mean_xp;
  double Sigma_mean_yp;
  double Npart;
  double Sigma_emitt_x;
  double Sigma_emitt_y;
  double Sigma_beta_x;
  double Sigma_beta_y;
  double Sigma_alph_x;
  double Sigma_alph_y;
  double Sigma_sigma_x;
  double Sigma_sigma_y;
  double Sigma_sigma_xp;
  double Sigma_sigma_yp;
  double Sigma_disp_x;
  double Sigma_disp_y;
  double Sigma_disp_xp;
  double Sigma_disp_yp;
};

#endif


