#ifndef RoBdsimOutput_h
#define RoBdsimOutput_h

#include <vector>
#include <map>
#include <string>

class TChain;
class TFile;
class TH1;
class TH2;

class Analysis;
class OutputOptics;
class Sampler;
class SamplerAnalysis;

class RobdsimOutput
{
 public : 
  RobdsimOutput();
  RobdsimOutput(const char *path, const char *outputName, bool debug=false);   
  ~RobdsimOutput(); 

  void                     CommonConstructor(); 
  void                     FileStructure();

  void                     MakeVectorOfRootFiles(const char *path); 
  std::vector<std::string> GetVectorOfRootFileNames()      { return rootFiles; }
  std::vector<std::string> GetVectorOfSamplerTreeNames()   { return samplerNames;}
  std::vector<std::string> GetVectorOfTreeNames()          { return treeNames;} 
  std::vector<std::string> GetVectorOfHistoNames()         { return histoNames;} 
  
  std::map<std::string,TChain*> GetMapOfChains()           { return treeChains;}
  std::vector<TChain*>          GetVectorOfSamplerChains() { return samplerChains;}
  
  TChain*                  GetChain(std::string chainName);
  TChain*                  GetSamplerChain(int iSampler); 
  TChain*                  GetSamplerChain(std::string samplerName);
  SamplerAnalysis*         GetSamplerAnalysis(int iSampler); 
  SamplerAnalysis*         GetSamplerAnalysis(std::string samplerName);  
    
  void                     Chain();
  void                     AssignStructures(); 
  void                     RebuildSamplerChains();
    
  void                     TreeLoop(std::string treeName);                       ///<template for looping over non-sampler chains
  void                     SamplerLoop();                                        ///<template for looping over all samplers 
  void                     FillHisto(std::string tree, std::string histoName, std::string nbins, std::string binning, std::string plot, std::string selection);
  void                     CalculateOpticalFunctions(std::string outputFilename);///<sets optics function calculation 

  void                     WriteOutput(std::string outputFilename);
  
 private:
  /// Initialise all variables - do in a function as multiple constructors
  void InitialiseVariables();
  
  // Internal variables 
  bool   debug;
  //  char   *path;

  std::vector<std::string>          rootFiles; 
  std::map<std::string, TFile*>     rootFilesMap;

  std::vector<std::string>          treeNames;
  std::map<std::string,std::string> treeTypes;
  std::map<std::string,TChain*>     treeChains; 
  std::map<std::string,void *>      treeStructures;
  std::map<std::string, Analysis *> treeAnalyses;

  std::vector<std::string>          samplerNames;
  std::vector<TChain*>              samplerChains;
  std::vector<Sampler*>             samplers;
  std::vector<SamplerAnalysis*>     samplerAnalyses;

  std::vector<std::string>          histoNames;
  std::vector<std::string>          histoNames2D;
  std::map<std::string, TH1*>       histos;
  std::map<std::string, TH2*>       histos2D;
  bool                              extraHistos;

  OutputOptics*                     outputOptics;

  TFile*                            outputFile;

};

#endif
