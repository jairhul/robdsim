#ifndef RobdsimAnalysis_h
#define RobdsimAnalysis_h

#include <string>

class AnalysisConfig;
class RobdsimOutput;

class RobdsimAnalysis
{
 public:
  RobdsimAnalysis(std::string fileName,
		  std::string dataFileName = "",
		  std::string outputFileName = "");
 private: 
  AnalysisConfig *c;
  RobdsimOutput *r;
};

#endif
