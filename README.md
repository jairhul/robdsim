# README #

ROOT/C++ code for accessing and processing BDSIM output data in the ROOT format

### What is this repository for? ###

Large files are written by BDSIM, ROOT is an analysis framework for high energy physics data. BDSIM can output ROOT files and this repository gives users the ability to load and process the data via:

- C++
- Executing ROOT scripts 
- Python

### How do I get set up? ###
The code is effectively a shared library (.so) which can be loaded by analysis environments. 

## Build (with CMake) ##
```
mkdir build
cmake ..
make
```

## In ROOT ##
```
#!verbatim
cd ${ROBDSIMDIR}/build
root -l
root [0] .L librobdsim.so
```

## In PYTHON ##
```
#!python
export PYTHONPATH=${PYTHONPATH}:${ROBDSIMDIR}/build
python> import robdsim
```