#ifndef __Analysis_h
#define __Analysis_h

class Analysis {
 public : 
  Analysis();
  virtual ~Analysis();
 
  virtual void Init()    = 0;
  virtual void Process() = 0;
  virtual void Term()    = 0;
};


#endif
