# robdsim ctests

# _robdsimAnalTest:
#  runs robdsimAnal based on the config.txt file
#
# _robdsimcCompTest: 
#  runs robdsimComp based on the -ref.root file
#  
# robdsimTest:
#  Runs both robdsimAnalTest and robdsimCompTest after each other.

macro(_robdsimAnalTest testName configFile)
  add_test(NAME ${testName} COMMAND ${robdsimBinary} ${configFile})
endmacro()

macro(_robdsimCompTest testName testFile refFile)
  add_test(NAME ${testName} COMMAND ${robdsimCompBinary} ${testFile} ${refFile})
endmacro()

macro(robdsimTest testName)
    _robdsimAnalTest(${testName}_robdsimAnal ${testName}.txt)
    _robdsimCompTest(${testName}_robdsimComp ${testName}_anal.root ${testName}_ref.root)
    # these test depends on the running of the original test (assume same name with _LONG which is a little hackish)
    set_tests_properties(${testName}_robdsimAnal PROPERTIES DEPENDS ${testName}_LONG)
    set_tests_properties(${testName}_robdsimComp PROPERTIES DEPENDS ${testName}_robdsimAnal)
endmacro()
