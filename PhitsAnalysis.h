#ifndef __PhitsAnalysis_h
#define __PhitsAnalysis_h

#include <iostream>

#include "Analysis.h"
#include "Phits.h"

class PhitsAnalysis : public Analysis { 
 public: 
  PhitsAnalysis(Phits *sIn);
  ~PhitsAnalysis();

  void Init();
  void Process(); 
  void Term();
 protected: 
  Phits *s;
};

#endif
