import ROOT
import os as _os
import subprocess as _subprocess

#get the directory and filename of this module
_this_dir, _this_filename = _os.path.split(__file__)


#compile the C code if there's no .so file found
if not _os.path.isfile(_os.path.join(_this_dir, 'librobdsim.so')):
    _p = _subprocess.Popen(['make'], cwd=_this_dir)
    _p.wait()

#load the .so in root from the package directory, where ever that is
ROOT.gSystem.Load(_os.path.join(_this_dir, 'librobdsim.so'))

#import the library from root to the current namespace
from ROOT import RobdsimOutput
from ROOT import RobdsimAnalysis
from ROOT import AnalysisConfig

