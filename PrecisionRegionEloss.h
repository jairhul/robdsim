//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Aug 12 13:51:42 2015 by ROOT version 5.34/32
// from TTree PrecisionRegionElossTree/Energy Loss
// found on file: QUAD.root
//////////////////////////////////////////////////////////

#ifndef PrecisionRegionEloss_h
#define PrecisionRegionEloss_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class PrecisionRegionEloss {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Float_t         X;
   Float_t         Y;
   Float_t         Z;
   Float_t         S;
   Float_t         x;
   Float_t         y;
   Float_t         z;
   Float_t         E;
   Float_t         weight;
   Int_t           partID;
   Char_t          volumeName;
   Int_t           turnnumber;
   Int_t           eventNo;

   // List of branches
   TBranch        *b_X;   //!
   TBranch        *b_Y;   //!
   TBranch        *b_Z;   //!
   TBranch        *b_S;   //!
   TBranch        *b_x;   //!
   TBranch        *b_y;   //!
   TBranch        *b_z;   //!
   TBranch        *b_E;   //!
   TBranch        *b_weight;   //!
   TBranch        *b_partID;   //!
   TBranch        *b_volumeName;   //!
   TBranch        *b_turnnumber;   //!
   TBranch        *b_eventNo;   //!

   PrecisionRegionEloss(TTree *tree=0);
   virtual ~PrecisionRegionEloss();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef PrecisionRegionEloss_cxx
PrecisionRegionEloss::PrecisionRegionEloss(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("QUAD.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("QUAD.root");
      }
      f->GetObject("PrecisionRegionElossTree",tree);

   }
   Init(tree);
}

PrecisionRegionEloss::~PrecisionRegionEloss()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t PrecisionRegionEloss::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t PrecisionRegionEloss::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void PrecisionRegionEloss::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("X", &X, &b_X);
   fChain->SetBranchAddress("Y", &Y, &b_Y);
   fChain->SetBranchAddress("Z", &Z, &b_Z);
   fChain->SetBranchAddress("S", &S, &b_S);
   fChain->SetBranchAddress("x", &x, &b_x);
   fChain->SetBranchAddress("y", &y, &b_y);
   fChain->SetBranchAddress("z", &z, &b_z);
   fChain->SetBranchAddress("E", &E, &b_E);
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("partID", &partID, &b_partID);
   fChain->SetBranchAddress("volumeName", &volumeName, &b_volumeName);
   fChain->SetBranchAddress("turnnumber", &turnnumber, &b_turnnumber);
   fChain->SetBranchAddress("eventNo", &eventNo, &b_eventNo);
   Notify();
}

Bool_t PrecisionRegionEloss::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void PrecisionRegionEloss::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t PrecisionRegionEloss::Cut(Long64_t /*entry*/)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef PrecisionRegionEloss_cxx
