#ifndef __PrecisionRegionElossAnalysis_h
#define __PrecisionRegionElossAnalysis_h

#include <iostream>

#include "Analysis.h"
#include "PrecisionRegionEloss.h"

class PrecisionRegionElossAnalysis : public Analysis { 
 public: 
  PrecisionRegionElossAnalysis(PrecisionRegionEloss *eIn);
  ~PrecisionRegionElossAnalysis();

  void Init();
  void Process(); 
  void Term();
 protected: 
  PrecisionRegionEloss *e;
};

#endif
